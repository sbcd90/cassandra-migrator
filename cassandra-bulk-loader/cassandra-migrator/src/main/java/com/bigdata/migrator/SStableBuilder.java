package com.bigdata.migrator;

import com.google.common.io.Resources;
import org.apache.cassandra.config.Config;
import org.apache.cassandra.dht.Murmur3Partitioner;
import org.apache.cassandra.io.sstable.CQLSSTableWriter;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class SStableBuilder {
    private static final String csvFileName = Resources.getResource("employee.csv").getFile();

    private static final String KEYSPACE = "migrator";

    private static final String TABLE = "employee";

    private static final String SCHEMA = String.format("CREATE TABLE %s.%s (" +
                                                        "id int," +
                                                        "name text," +
                                                        "PRIMARY KEY (id) )", KEYSPACE, TABLE);

    private static final String INSERT_STMT = String.format("INSERT INTO %s.%s (" +
                                                            "id, name" +
                                                            ") VALUES (" +
                                                            "?, ?" +
                                                            ")", KEYSPACE, TABLE);

    public static void main(String[] args) {
        Config.setClientMode(true);

        File outputDir = new File(KEYSPACE + File.separator + TABLE);

        if (!outputDir.exists()) {
            if (!outputDir.mkdirs()) {
                throw new RuntimeException("Cannot create location for storing sstables");
            }
        }

        CQLSSTableWriter.Builder builder = CQLSSTableWriter.builder();

        builder.inDirectory(outputDir)
                .forTable(SCHEMA)
                .using(INSERT_STMT)
                .withPartitioner(new Murmur3Partitioner());
        CQLSSTableWriter writer = builder.build();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(csvFileName));
            CsvListReader csvListReader = new CsvListReader(reader, CsvPreference.STANDARD_PREFERENCE);

            List<String> line;
            while ((line = csvListReader.read()) != null) {
                writer.addRow(Integer.parseInt(line.get(0)),
                              line.get(1));
            }
        } catch (IOException e) {
            throw new RuntimeException("Cannot Bulk load data", e);
        }

        try {
            writer.close();
        } catch (IOException ignore) {}
    }
}