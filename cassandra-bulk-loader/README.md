cassandra bulk-loader
=====================

- Steps:

1. Download Sigar Library from [link](http://sourceforge.net/projects/sigar/files/sigar/1.6/hyperic-sigar-1.6.4.zip/download) & add `/sigar-bin/lib` to the path.

1. Download the project cassandra-migrator. Open it & compile it with maven.

```
mvn clean install
```

2. Run the Java application SStableBuilder. (main method)

3. This creates a set of files under `migrator/employee` directory.

4. Open windows cmd & navigate to the location where `sstableloader.bat` file is present.

5. Execute the following command to upload bulk data to cassandra.

```
sstableloader.bat -d <host where cassandra server runs> <location of the generated files>
```