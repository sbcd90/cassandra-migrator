cassandra-migrator
==================

- This project demonstrates various ways to migrate data into Apache Cassandra

- Steps

1. Install Cassandra from Datastax from the [link](http://www.planetcassandra.org/cassandra/)

    The steps are self-explanatory.
    
2. Follow the README sections below mentioned to upload data to Cassandra.

    COPY command - [link](https://bitbucket.org/sbcd90/cassandra-migrator/src/2f9daf585a7202c2bd48a5c8216883722d93f743/cassandra-copy-command/README.md?at=master&fileviewer=file-view-default)
    Cassandra Bulk Loader - [link](https://bitbucket.org/sbcd90/cassandra-migrator/src/2f9daf585a7202c2bd48a5c8216883722d93f743/cassandra-bulk-loader/README.md?at=master&fileviewer=file-view-default)