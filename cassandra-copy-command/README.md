cassandra-copy-command
======================

- Steps

1. In `copyFromCsv.cql` file, adjust the location of `employee.csv` file.

2. Navigate to the location where `cqlsh.bat` is present & execute it.

3. In the command line shell execute the following command

```
source <path to copyFromCsv.cql>
```